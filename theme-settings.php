<?php
/* $Id */

require_once './' . drupal_get_path('theme', 'pop') . "/internals/template.theme-registry.inc";

/**
 * Initialize theme settings
 */
function _pop_theme($theme) {
  if ($theme == 'pop') { // only needs to be run once
    // Compute the conditional stylesheets
    if (!module_exists('conditional_styles')) {
      if (_theme_load_include('inc', $theme, 'internals/template.conditional-styles')) {
        _conditional_styles_theme($existing, $type, $theme, $path);
      }
    }
    // Compute the conditional scripts
    if (!module_exists('conditional_scripts')) {
      if (_theme_load_include('inc', $theme, 'internals/template.conditional-scripts')) {
        _conditional_scripts_theme($existing, $type, $theme, $path);
      }
    }
  }
    // Get node types
    $defaults = _pop_get_default_settings($theme);

    // Get default theme settings.
    $settings = theme_get_settings($theme);
    _pop_reset_by_content_type_settings($settings, $defaults);

    // Don't save the toggle_node_info_ variables
    if (module_exists('node')) {
      foreach (node_get_types() as $type => $name) {
        unset($settings['toggle_node_info_'. $type]);
      }
    }

    // Save default theme settings
    variable_set(
      str_replace('/', '_', 'theme_'. $theme .'_settings'),
      array_merge($defaults, $settings)
    );

    // If the active theme has been loaded, force refresh of Drupal internals.
    if (!empty($GLOBALS['theme_key'])) {
      theme_get_setting('', TRUE);
    }
//  }
}

/**
 * Implements THEMEHOOK_settings() function.
 *
 * @param $saved_settings
 *   An array of saved settings for this theme.
 * @return
 *   A form array.
 */
function pop_settings($saved_settings, $subtheme_defaults = array()) {
  // FIXME: base theme setting not inherited
  // http://drupal.org/node/481142, http://drupal.org/node/563708

  // Add javascript to show/hide optional settings
  drupal_add_js(drupal_get_path('theme', 'pop') . '/scripts/theme-settings.js', 'theme');

  // Get the default values.
  $defaults = _pop_get_default_settings('pop');

  // Allow a subtheme to override the default values.
  $defaults = array_merge($defaults, $subtheme_defaults);

  // Merge the saved variables and their default values.
  $settings = array_merge($defaults, $saved_settings);

  // Reset the values if per content-type settings are not enabled
  _pop_reset_by_content_type_settings($settings, $defaults);

  // Create the form using Forms API: http://api.drupal.org/api/6
  $form = array();

  // Pop specific settings
  $form['logo']['logo_path']['#description'] = $form['logo']['logo_path']['#description']
    . t(' The logo size should be 36x120 px (height x width) maximum.');

  $form['pop'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('General'),
    '#attributes'    => array('id' => 'pop-settings'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );


  // Primary menu dropdown settings
  $form['pop']['primary_menu_dropdown'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show primary menu as dropdown'),
    '#description'   => t('Uses superfish menu by default.'),
    '#default_value' => $settings['primary_menu_dropdown'],
  );


  // Breadcrumb settings
  $form['breadcrumb'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Breadcrumb'),
    '#attributes'    => array('id' => 'pop-breadcrumb'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );
  $form['breadcrumb']['breadcrumb'] = array(
    '#type'          => 'select',
    '#title'         => t('Display breadcrumb'),
    '#default_value' => $settings['breadcrumb'],
    '#options'       => array(
                          'yes'   => t('Yes'),
                          'admin' => t('Only in admin section'),
                          'no'    => t('No'),
                        ),
  );
  $form['breadcrumb']['breadcrumb_separator'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Breadcrumb separator'),
    '#description'   => t('Text only. Don\'t forget to include additional spaces in front and at the end.'),
    '#default_value' => t(filter_xss_admin($settings['breadcrumb_separator'])),
    '#size'          => 5,
    '#maxlength'     => 10,
    '#attributes'    => array('class' => 'div-breadcrumb-collapse'), // jquery hook to show/hide optional widgets
  );
  $form['breadcrumb']['breadcrumb_home'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show home page link in breadcrumb'),
    '#default_value' => $settings['breadcrumb_home'],
    '#attributes'    => array('class' => 'div-breadcrumb-collapse'), // jquery hook to show/hide optional widgets
  );
  $form['breadcrumb']['breadcrumb_trailing'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append a separator to the end of the breadcrumb'),
    '#default_value' => $settings['breadcrumb_trailing'],
    '#description'   => t('Useful when the breadcrumb is placed just before the title.'),
    '#attributes'    => array('class' => 'div-breadcrumb-collapse'), // jquery hook to show/hide optional widgets
  );
  $form['breadcrumb']['breadcrumb_title'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append the content title to the end of the breadcrumb'),
    '#default_value' => $settings['breadcrumb_title'],
    '#description'   => t('Useful when the breadcrumb is not placed just before the title.'),
    '#attributes'    => array('class' => 'div-breadcrumb-collapse'), // jquery hook to show/hide optional widgets
  );


  // Node settings
  $form['node'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Node'),
    '#description'   => t('These settings apply to all content types, or check the "Use custom settings" box below for each content type. For example, you may want to show the date on stories, but not pages.<br />Note that "Display post information on" settings under Global Settings are ignored.'),
    '#attributes'    => array('id' => 'pop-node'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );
  // author & date default
  $form['node']['default']['#collapsed'] = $settings['node_by_content_type'] ? TRUE : FALSE;
  $form['node']['default'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('!name', array('!name' => t('Default'))),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
    '#attributes'    => array('id' => 'pop-node-default'),
  );
  _pop_prepare_node_form($form, $settings);

  $form['node']['node_by_content_type'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use custom settings for each content type instead of the default above'),
    '#default_value' => $settings['node_by_content_type'],
  );


  // Node per content-type settings
  $node_types = node_get_types('names');

  foreach ($node_types as $type => $name) {
    $form['node'][$type] = array(
      '#type'         => 'fieldset',
      '#title'        => t('!name', array('!name' => t($name))),
      '#collapsible'  => TRUE,
      '#collapsed'    => TRUE,
      '#attributes'    => array('class' => 'div-node-collapse'),
    );

    _pop_prepare_node_form($form, $settings, $type);
  }


  // Taxonomy settings
  if (module_exists('taxonomy')) {
    $form['taxonomy'] = array(
      '#type'          => 'fieldset',
      '#title'         => t('Taxonomy'),
      '#description'   => t('These settings apply to all content types, or check the "Use custom settings" box below for each content type.'),
      '#attributes'    => array('id' => 'pop-taxonomy'),
      '#collapsible'   => TRUE,
      '#collapsed'     => TRUE,
    );
    // taxonomy default
    $form['taxonomy']['default']['#collapsed'] = $settings['taxonomy_by_content_type'] ? TRUE : FALSE;
    $form['taxonomy']['default'] = array(
      '#type'          => 'fieldset',
      '#title'         => t('!name', array('!name' => t('Default'))),
      '#collapsible'   => TRUE,
      '#collapsed'     => FALSE,
      '#attributes'    => array('id' => 'pop-taxonomy-default'),
    );

    _pop_prepare_taxonomy_form($form, $settings);

    $form['taxonomy']['taxonomy_by_content_type'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Use custom settings for each content type instead of the default above'),
      '#default_value' => $settings['taxonomy_by_content_type'],
    );


    // Taxonomy per content-type settings
    foreach ($node_types as $type => $name) {
      // Options for default settings
      $form['taxonomy'][$type] = array(
        '#type'          => 'fieldset',
        '#title'         => t('!name', array('!name' => t($name))),
        '#collapsible'   => TRUE,
        '#collapsed'     => TRUE,
        '#attributes'    => array('class' => 'div-taxonomy-collapse'),
      );

      _pop_prepare_taxonomy_form($form, $settings, $type);

    }
  }


  // Read more settings
  $form['readmore'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Read more'),
    '#description'   => t('These settings apply to all content types, or check the "Use custom settings" box below for each content type.'),
    '#attributes'    => array('id' => 'pop-readmore'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );
  // readmore default
  $form['readmore']['default']['#collapsed'] = $settings['readmore_by_content_type'] ? TRUE : FALSE;
  $form['readmore']['default'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('!name', array('!name' => t('Default'))),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
    '#attributes'    => array('id' => 'pop-readmore-default'),
  );

  _pop_prepare_readmore_form($form, $settings);

  $form['readmore']['readmore_by_content_type'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use custom settings for each content type instead of the default above'),
    '#default_value' => $settings['readmore_by_content_type'],
  );


  // Read more per content-type settings
  foreach ($node_types as $type => $name) {
    $form['readmore'][$type] = array(
      '#type'         => 'fieldset',
      '#title'        => t('!name', array('!name' => t($name))),
      '#collapsible'  => TRUE,
      '#collapsed'    => TRUE,
      '#attributes'    => array('class' => 'div-readmore-collapse'),
    );

    _pop_prepare_readmore_form($form, $settings, $type);
  }


  // Comment settings
  $form['comment'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Comment'),
    '#description'   => t('These settings apply to all content types, or check the "Use custom settings" box below for each content type.'),
    '#attributes'    => array('id' => 'pop-comment'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );
  // comment wrapper header
  $form['comment']['comment_header'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Comments header'),
    '#default_value' => t(filter_xss_admin($settings['comment_header'])),
    '#description'   => t('The header text to appear on top of comments area. HTML is allowed.'),
  );
  // comment default
  $form['comment']['default']['#collapsed'] = $settings['comment_by_content_type'] ? TRUE : FALSE;
  $form['comment']['default'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('!name', array('!name' => t('Default'))),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
    '#attributes'    => array('id' => 'pop-comment-default'),
  );

  _pop_prepare_comment_form($form, $settings);

  $form['comment']['comment_by_content_type'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use custom settings for each content type instead of the default above'),
    '#default_value' => $settings['comment_by_content_type'],
  );


  // Comment per content-type settings
  foreach ($node_types as $type => $name) {
    $form['comment'][$type] = array(
      '#type'         => 'fieldset',
      '#title'        => t('!name', array('!name' => t($name))),
      '#collapsible'  => TRUE,
      '#collapsed'    => TRUE,
      '#attributes'    => array('class' => 'div-comment-collapse'),
    );

    _pop_prepare_comment_form($form, $settings, $type);
  }


  // Search box settings
  $form['search_box'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Search box'),
    '#description' => t('The search box customization.'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );
  $form['search_box']['search_box_label'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Display search box label'),
    '#default_value' => $settings['search_box_label'],
  );
  $form['search_box']['search_box_label_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Search box label'),
    '#description'   => t('Text only. Default is "Search this site".'),
    '#default_value' => t(filter_xss_admin($settings['search_box_label_text'])),
    '#size'          => 30,
    '#maxlength'     => 255,
  );
  $form['search_box']['search_box_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Search box default text'),
    '#description'   => t('Text only. Put "&lt;none&gt;" to disable it.'),
    '#default_value' => t(filter_xss_admin($settings['search_box_text'])),
    '#size'          => 30,
    '#maxlength'     => 255,
  );
  $form['search_box']['search_box_tooltip_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Search box tooltip, i.e. input field\'s title value'),
    '#description'   => t('Text only. Default is "Enter the terms you wish to search for.".'),
    '#default_value' => t(filter_xss_admin($settings['search_box_tooltip_text'])),
    '#size'          => 30,
    '#maxlength'     => 255,
  );
  $form['search_box']['search_box_button'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display the search button'),
    '#default_value' => $settings['search_box_button'],
  );
  $form['search_box']['search_box_button_image'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use image button. Using "images/search.png" under theme folder by default.'),
    '#default_value' => $settings['search_box_button_image'],
  );
  $form['search_box']['search_box_button_text'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Search box button text'),
    '#description'   => t('Text only. Default is "Go".'),
    '#default_value' => t(filter_xss_admin($settings['search_box_button_text'])),
    '#size'          => 20,
    '#maxlength'     => 30,
  );


  // Help toggle settings
  $form['help'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Help toggle'),
    '#attributes'    => array('id' => 'pop-help'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );
  $form['help']['help_toggle'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable help toggle button'),
    '#description'   => t('Toggle the display of help message.'),
    '#default_value' => $settings['help_toggle'],
  );
  $form['help']['help_hide_default'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Start hidden'),
    '#description'   => t('Help message is hidden initially.'),
    '#default_value' => $settings['help_hide_default'],
  );


  // Return the form
  return $form;
}


/* Node settings form */
function _pop_prepare_node_form(&$form, $settings, $type = 'default') {
  $form['node'][$type]['node_show_author_' . $type] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Display author\'s username'),
    '#default_value' => $settings['node_show_author_' . $type],
  );
  $form['node'][$type]['node_show_date_' . $type] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Display date posted (you can customize this format on your !link page)', array('!link' => l(t('Date and Time settings'), 'admin/settings/date-time'))),
    '#default_value' => $settings['node_show_date_' . $type],
  );
}

/* Taxonomy settings form */
function _pop_prepare_taxonomy_form(&$form, $settings, $type = 'default') {
  $form['taxonomy'][$type]['taxonomy_display_' . $type] = array(
      '#type'          => 'select',
      '#title'         => t('When to display taxonomy terms'),
      '#default_value' => $settings['taxonomy_display_' . $type],
      '#options'       => array(
                            'never' => t('Never display taxonomy terms'),
                            'all'   => t('Always display taxonomy terms'),
                            'fullnode'  => t('Only display taxonomy terms on full node pages'),
                          ),
  );
  $form['taxonomy'][$type]['taxonomy_format_' . $type] = array(
      '#type'          => 'select',
      '#title'         => t('Taxonomy display format'),
      '#default_value' => $settings['taxonomy_format_' . $type],
      '#options'       => array(
                            'vocab' => t('Display each vocabulary on a new line'),
                            'list'  => t('Display all taxonomy terms together in single list'),
                          ),
  );
  // Get taxonomy vocabularies by node type
  $vocabs = array();
  $vocabs_by_type = ($type == 'default') ? taxonomy_get_vocabularies() : taxonomy_get_vocabularies($type);
  foreach ($vocabs_by_type as $key => $value) {
    $vocabs[$value->vid] = $value->name;
  }
  // Display taxonomy checkboxes
  foreach ($vocabs as $key => $vocab_name) {
    $form['taxonomy'][$type]['taxonomy_vocab_display_' . $type . '_' . $key] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Display vocabulary: '. $vocab_name),
      '#default_value' => $settings['taxonomy_vocab_display_' . $type . '_' . $key],
    );
  }
}

/* Read more settings form */
function _pop_prepare_readmore_form(&$form, $settings, $type = 'default') {
  $form['readmore'][$type]['readmore_' . $type] = array(
    '#type'          => 'textfield',
    '#title'         => t('Link text'),
    '#default_value' => t(filter_xss_admin($settings['readmore_' . $type])),
    '#description'   => t('HTML is allowed.'),
  );
  $form['readmore'][$type]['readmore_title_' . $type] = array(
    '#type'          => 'textfield',
    '#title'         => t('Title text (tool tip)'),
    '#default_value' => t(filter_xss_admin($settings['readmore_title_' . $type])),
    '#description'   => t('Displayed when hovering over link. Plain text only.'),
  );
}

/* Comment settings form */
function _pop_prepare_comment_form(&$form, $settings, $type = 'default') {
  $form['comment'][$type]['add'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('"Add new comment" link'),
    '#description' => t('The link when there are no comments.'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );
  // full node
  $form['comment'][$type]['add']['node'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('For full node'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );
  $form['comment'][$type]['add']['node']['comment_node_' . $type] = array(
    '#type'          => 'textfield',
    '#title'         => t('Link text'),
    '#default_value' => $settings['comment_node_' . $type],
    '#description'   => t('HTML is allowed.'),
  );
  $form['comment'][$type]['add']['node']['comment_node_title_' . $type] = array(
    '#type'          => 'textfield',
    '#title'         => t('Title text (tool tip)'),
    '#default_value' => t(filter_xss_admin($settings['comment_node_title_' . $type])),
    '#description'   => t('Displayed when hovering over link. Plain text only.'),
  );
  // teaser
  $form['comment'][$type]['add']['teaser'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('For teaser'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );
  $form['comment'][$type]['add']['teaser']['comment_add_' . $type] = array(
    '#type'          => 'textfield',
    '#title'         => t('Link text'),
    '#default_value' => $settings['comment_add_' . $type],
    '#description'   => t('HTML is allowed.'),
  );
  $form['comment'][$type]['add']['teaser']['comment_add_title_' . $type] = array(
    '#type'          => 'textfield',
    '#title'         => t('Title text (tool tip)'),
    '#default_value' => $settings['comment_add_title_' . $type],
    '#description'   => t('Displayed when hovering over link. Plain text only.'),
  );
  $form['comment'][$type]['standard'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('"Comments" link'),
    '#description' => t('The link when there are one or more comments.'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );
  $form['comment'][$type]['standard']['comment_singular_' . $type] = array(
    '#type'          => 'textfield',
    '#title'         => t('Link text when there is 1 comment'),
    '#default_value' => t(filter_xss_admin($settings['comment_singular_' . $type])),
    '#description'   => t('HTML is allowed.'),
  );
  $form['comment'][$type]['standard']['comment_plural_' . $type] = array(
    '#type'          => 'textfield',
    '#title'         => t('Link text when there are multiple comments'),
    '#default_value' => t(filter_xss_admin($settings['comment_plural_' . $type])),
    '#description'   => t('HTML is allowed. @count will be replaced with the number of comments.'),
  );
  $form['comment'][$type]['standard']['comment_title_' . $type] = array(
    '#type'          => 'textfield',
    '#title'         => t('Title text (tool tip)'),
    '#default_value' => t(filter_xss_admin($settings['comment_title_' . $type])),
    '#description'   => t('Displayed when hovering over link. Plain text only.'),
  );
  $form['comment'][$type]['new'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('"New comments" link'),
    '#description' => t('The link when there are one or more new comments.'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );
  $form['comment'][$type]['new']['comment_new_singular_' . $type] = array(
    '#type'          => 'textfield',
    '#title'         => t('Link text when there is 1 new comment'),
    '#default_value' => t(filter_xss_admin($settings['comment_new_singular_' . $type])),
    '#description'   => t('HTML is allowed.'),
  );
  $form['comment'][$type]['new']['comment_new_plural_' . $type] = array(
    '#type'          => 'textfield',
    '#title'         => t('Link text when there are multiple new comments'),
    '#default_value' => t(filter_xss_admin($settings['comment_new_plural_' . $type])),
    '#description'   => t('HTML is allowed. @count will be replaced with the number of comments.'),
  );
  $form['comment'][$type]['new']['comment_new_title_' . $type] = array(
    '#type'          => 'textfield',
    '#title'         => t('Title text (tool tip)'),
    '#default_value' => t(filter_xss_admin($settings['comment_new_title_' . $type])),
    '#description'   => t('Displayed when hovering over link. Plain text only.'),
  );
}
