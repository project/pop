<?php
/* $Id */

/**
 * @file node.tpl.php
 *
 * Altered template for node.
 *
 * Released under the GNU General Public License.
 */
?>

<?php if (empty($hide)): ?>

<div <?php if (!empty($attr)) print drupal_attributes($attr) ?>>
  <?php if ((!empty($title)) && !$page): ?>
    <h2 class='<?php print $hook ?>-title'>
      <?php if (!empty($new)): ?><a id='new' class='new'><?php print('New') ?></a><?php endif; ?>
      <a href="<?php print $node_url ?>"><?php print $title ?></a>
    </h2>
  <?php endif; ?>

  <?php if (!empty($submitted)): ?>
    <div class="<?php print $hook ?>-submitted clearfix"><?php print $submitted ?></div>
  <?php endif; ?>

  <?php if (!empty($content)): ?>
    <div class="<?php print $hook ?>-content clearfix">
      <?php print $content ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($links)): ?>
    <div class="<?php print $hook ?>-links clearfix"><?php print $links ?></div>
  <?php endif; ?>
</div>

<?php endif; ?>