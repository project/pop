<?php
/**
 * @file
 * Contains default theme registry settings.
 * Released under the GNU General Public License.
 */

/**
 * Return the theme settings' default values and save them into the database.
 *
 * @param $theme
 *   The name of theme.
 */
function _pop_get_default_settings($theme) {
  $themes = list_themes();

  // Get the default values from the .info file.
  $defaults = !empty($themes[$theme]->info['settings']) ? $themes[$theme]->info['settings'] : array();

  if (!empty($defaults)) {
    // Make the default content-type settings the same as the default theme settings,
    // so we can tell if content-type-specific settings have been altered.
    $defaults = array_merge($defaults, variable_get('theme_'. $theme .'_settings', array()));

    // Get the node types
    $node_types = node_get_types('names');

    // Set the default values for per content-type settings
    foreach ($node_types as $type => $name) {
      $defaults['node_show_author_' . $type]     = $defaults['node_show_author_default'];
      $defaults['node_show_date_' . $type]       = $defaults['node_show_date_default'];
      $defaults['taxonomy_display_' . $type]     = $defaults['taxonomy_display_default'];
      $defaults['taxonomy_format_' . $type]      = $defaults['taxonomy_format_default'];
      $defaults['comment_singular_' . $type]     = $defaults['comment_singular_default'];
      $defaults['comment_plural_' . $type]       = $defaults['comment_plural_default'];
      $defaults['comment_title_' . $type]        = $defaults['comment_title_default'];
      $defaults['comment_new_singular_' . $type] = $defaults['comment_new_singular_default'];
      $defaults['comment_new_plural_' . $type]   = $defaults['comment_new_plural_default'];
      $defaults['comment_new_title_' . $type]    = $defaults['comment_new_title_default'];
      $defaults['comment_add_' . $type]          = $defaults['comment_add_default'];
      $defaults['comment_add_title_' . $type]    = $defaults['comment_add_title_default'];
      $defaults['comment_node_' . $type]         = $defaults['comment_node_default'];
      $defaults['comment_node_title_' . $type]   = $defaults['comment_node_title_default'];
      $defaults['readmore_' . $type]             = $defaults['readmore_default'];
      $defaults['readmore_title_' . $type]       = $defaults['readmore_title_default'];
    }
  }
  // Return the default settings.
  return $defaults;
}

function _pop_reset_by_content_type_settings(&$settings, &$defaults) {
  // Get the node types
  $node_types = node_get_types('names');

  // If content type-specifc settings are not enabled, reset the values
  if ($settings['readmore_by_content_type'] == 0) {
    foreach ($node_types as $type => $name) {
      $settings['readmore_' . $type]           = $defaults['readmore_default'];
      $settings['readmore_title_' . $type]     = $defaults['readmore_title_default'];
    }
  }
  if ($settings['comment_by_content_type'] == 0) {
    foreach ($node_types as $type => $name) {
      $settings['comment_singular_' . $type]     = $defaults['comment_singular_default'];
      $settings['comment_plural_' . $type]       = $defaults['comment_plural_default'];
      $settings['comment_title_' . $type]        = $defaults['comment_title_default'];
      $settings['comment_new_singular_' . $type] = $defaults['comment_new_singular_default'];
      $settings['comment_new_plural_' . $type]   = $defaults['comment_new_plural_default'];
      $settings['comment_new_title_' . $type]    = $defaults['comment_new_title_default'];
      $settings['comment_add_' . $type]          = $defaults['comment_add_default'];
      $settings['comment_add_title_' . $type]    = $defaults['comment_add_title_default'];
      $settings['comment_node_' . $type]         = $defaults['comment_node_default'];
      $settings['comment_node_title_' . $type]   = $defaults['comment_node_title_default'];
    }
  }
}
