<?php
// conditional_scripts
// Released under the GNU General Public License.
// $Id$

/**
 * @file
 * Allows themes to add conditional stylesheets.
 *
 * @see http://msdn.microsoft.com/en-us/library/ms537512.aspx
 */

/**
 * Return paths for the theme and its base themes.
 *
 * @param $theme
 *   The name of the theme.
 * @return
 *   An array of all the theme paths.
 */
function conditional_scripts_paths_to_basetheme($theme) {
  static $theme_paths;
  if (empty($theme_paths[$theme])) {
    $theme_paths[$theme] = array();
    $themes = list_themes();
    // Grab the paths from the base theme.
    if (!empty($themes[$theme]->base_theme)) {
      $theme_paths[$theme] = conditional_scripts_paths_to_basetheme($themes[$theme]->base_theme);
    }
    $theme_paths[$theme][$theme] = dirname($themes[$theme]->filename);
  }
  return $theme_paths[$theme];
}

/**
 * When the theme registry is rebuilt, we also build the conditional scripts.
 */
function _conditional_scripts_theme($existing, $type, $theme, $path) {
  // @TODO: For PHP 4 compatibility we use foreach (array_keys($array) AS $key).
  // When PHP 5 becomes required (Drupal 7.x), use the following faster
  // implementation: foreach ($array AS $key => &$value) {}

  // Process the conditional scripts for every active theme.
  global $language;
  $themes = list_themes();
  foreach (array_keys($themes) AS $theme) {
    // We only need to process active themes.
    if ($themes[$theme]->status) {
      $paths = conditional_scripts_paths_to_basetheme($theme);

      // Grab all the conditional scripts.
      $scripts = array();
      // Start with the base theme and travel up the chain to the active theme.
      foreach ($paths AS $theme_name => $path) {
        // Look at the conditional-stylesheets defined in the theme's .info file.
        if (!empty($themes[$theme_name]->info['conditional-scripts'])) {
          foreach ($themes[$theme_name]->info['conditional-scripts'] AS $condition => $js) {
            // Allow the theme to override its base themes' javascripts.
            foreach ($js AS $media => $files) {
              foreach ($files AS $file) {
                $scripts[$condition][$media][$file] = $path;
              }
            }
          }
        }
      }
      // Render the scripts to script elements.
      $conditional_scripts = '';
      if (!empty($scripts)) {
        $query_string = '?'. substr(variable_get('css_js_query_string', '0'), 0, 1);
        $base_path = base_path();
        foreach ($scripts AS $condition => $js) {
          // Each condition requires its own set of script.
          $output = '';
          foreach ($js AS $media => $files) {
            foreach ($files AS $file => $path) {
              // Don't allow non-existent scripts to clutter the logs with 404.
              if (file_exists("./$path/$file")) {
                $output .= '<script type="text/javascript" src="'. $base_path . $path .'/'. $file . $query_string .'"></script>'. "\n";
              }
            }
          }
          if ($output) {
            $conditional_scripts .= "<!--[$condition]>\n$output<![endif]-->\n";
          }
        }
      }
      // Save the stylesheets for later retrieval.
      if ($conditional_scripts) {
        variable_set('conditional_scripts_' . $theme, $conditional_scripts);
      }
      else {
        variable_del('conditional_scripts_' . $theme);
      }
    }
  }

  // Return nothing.
  return array();
}
