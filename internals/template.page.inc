<?php
/**
 * @file
 * Contains functions only needed when rendering pages.
 * Released under the GNU General Public License.
 */

/**
 * Implementation of preprocess_page().
 */
function _pop_preprocess_page(&$vars) {
  $attr = array();
  $attr['class'] = $vars['body_classes'];

  if (isset($_GET['print'])) {
    _theme_load_include('inc', 'pop', 'internals/template.print');
  }

  // Replace screen/all stylesheets with print
  // We want a minimal print representation here for full control.
  if (isset($_GET['print'])) {
    $css = pop_strip_css();
    unset($css['all']);
    unset($css['screen']);
    $css['all'] = $css['print'];
    $vars['styles'] = drupal_get_css($css);

    // Add print header
    $vars['print_header'] = theme('print_header');

    // Replace all body classes
    $attr['class'] = 'print';

    // Use print template
    $vars['template_file'] = 'print-page';

    // Suppress devel output
    $GLOBALS['devel_shutdown'] = FALSE;
  }
  else {
    // Reduce unwanted css & js
//    $css_list = array('modules/*', '*/admin_menu/*');
//    $js_list = array('*/admin_menu/*');

    $vars['styles'] = drupal_get_css(pop_strip_css(/*$css_list*/));
    $vars['scripts'] = drupal_get_js('header', pop_strip_js(/*$js_list*/));
  }

  // Add conditional stylesheets & scripts
  if (!module_exists('conditional_styles')) {
    $vars['styles'] .= $vars['conditional_styles'] = variable_get('conditional_styles_' . $GLOBALS['theme'], '');
  }
  if (!module_exists('conditional_scripts')) {
    $vars['scripts'] .= $vars['conditional_scripts'] = variable_get('conditional_scripts_' . $GLOBALS['theme'], '');
  }

  // Add an optional title to the end of the breadcrumb.
  if (theme_get_setting('breadcrumb_title') && $vars['breadcrumb']) {
    $vars['breadcrumb'] = substr($vars['breadcrumb'], 0, -6) . $vars['title'] . '</div>';
  }

  // Split primary and secondary local tasks
  $vars['tabs'] = theme('menu_local_tasks', 'primary');
  $vars['tabs2'] = theme('menu_local_tasks', 'secondary');

  // We need to redo the $layout and body classes because
  // template_preprocess_page() assumes sidebars are named 'left' and 'right'.
  $vars['layout'] = 'none';
  if (!empty($vars['sidebar_first'])) {
    $vars['layout'] = 'first';
  }
  if (!empty($vars['sidebar_second'])) {
    $vars['layout'] = ($vars['layout'] == 'first') ? 'both' : 'second';
  }
  // If the layout is 'none', then template_preprocess_page() will already have
  // set a 'no-sidebars' class since it won't find a 'left' or 'right' sidebar.
  if ($vars['layout'] != 'none') {
    // Remove the incorrect 'no-sidebars' class.
    $attr['class'] = str_replace('no-sidebars', '', $attr['class']);

    // Set the proper layout body classes.
    if ($vars['layout'] == 'both') {
      $attr['class'] .= ' two-sidebars';
    }
    else {
      $attr['class'] .= ' one-sidebar sidebar-'. $vars['layout'];
    }
  }

  // Render all columns equal height by default
//  $attr['class'] .= ' equal';
  // enable for dark theme
  // $attr['class'] .= ' dark';

  // Don't render the attributes yet, so subthemes can alter them.
  $vars['attr'] = $attr;

  // User account links.
  $vars['user_links'] = _pop_user_links();

  // Remove help text if empty.
  if (empty($vars['help']) || !(strip_tags($vars['help']))) {
    unset($vars['help']);
  }

  // Help text toggle link.
  if (theme_get_setting('help_toggle')) {
    if ($vars['help']) {
      $class = theme_get_setting('help_hide_default') ? ' toggle-hidden' : ' toggle-open';
      $vars['help'] = preg_replace('/<div class="help/i', '<div class="help' . $class, $vars['help'], 1);
    }
    $vars['help_toggle'] = l(t('Help'), $_GET['q'], array('attributes' => array('id' => 'help-toggle', 'class' => 'toggle'), 'fragment' => 'help'));
  }

  // Generate superfish menu tree from source of primary links
  if ($vars['primary_links'] && theme_get_setting('primary_menu_dropdown') == 1) {
    // Check for menu internationalization
    if (module_exists('i18nmenu')) {
      $vars['primary_links_tree'] = i18nmenu_translated_tree(variable_get('menu_primary_links_source', 'primary-links'));
    }
    else {
      $vars['primary_links_tree'] = menu_tree(variable_get('menu_primary_links_source', 'primary-links'));
    }
    $vars['primary_links_tree'] = preg_replace('/<ul class="menu/i', '<ul class="sf-menu sf-shadow', $vars['primary_links_tree'], 1);
  }
  else {
    $vars['primary_links_tree'] = theme('links', $vars['primary_links'], array('class' => 'sf-menu sf-shadow'));
  }
}


/**
* Strip off unwanted CSS files.
*
* @param string $match
* @param string $exceptions
*/
function pop_strip_css($match = array('modules/*'), $exceptions = NULL) {
  // Set default exceptions
  if (!is_array($exceptions)) {
    $exceptions = array(
      'modules/system/system.css',
      'modules/update/update.css',
      'modules/openid/openid.css',
      'modules/acquia/*',
    );
  }

  $css = drupal_add_css();
  $match = implode("\n", $match);
  $exceptions = implode("\n", $exceptions);

  foreach (array_keys($css['all']['module']) as $filename) {
    if (drupal_match_path($filename, $match) && !drupal_match_path($filename, $exceptions)) {
      unset($css['all']['module'][$filename]);
    }
  }

  // Move the "all" CSS key to the front of the stack.
  ksort($css);
  return $css;
}

/**
* Strip off unwanted Javascript files.
*
* @param string $match
* @param string $exceptions
*/
function pop_strip_js($match = array(), $exceptions = NULL) {
  $js = drupal_add_js();
  $match = implode("\n", $match);
  if (!is_array($exceptions)) {
    $exceptions = array();
  }
  $exceptions = implode("\n", $exceptions);

  foreach (array_keys($js['module']) as $filename) {
    if (drupal_match_path($filename, $match) && !drupal_match_path($filename, $exceptions)) {
      unset($js['module'][$filename]);
    }
  }

  return $js;
}


/**
 * User/account-specific links.
 */
function _pop_user_links() {
  global $user;
  $user_links = array();
  if (!$user->uid) {
    $user_links['login'] = array('title' => t('Login'), 'href' => 'user');
    $user_links['register'] = array('title' => t('Register'), 'href' => 'user/register');
  }
  else {
    $user_links['account'] = array('title' => t('Hello !username', array('!username' => $user->name)), 'href' => 'user', 'html' => TRUE);
    $user_links['logout'] = array('title' => t('Logout'), 'href' => "logout");
  }
  return $user_links;
}
