<?php
/**
 * @file
 * Contains functions only needed when rendering pages.
 * Released under the GNU General Public License.
 */

/**
 * @file
 * Contains functions only needed if the user has block editing permissions.
 */

/**
 * Add block editing variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function _pop_preprocess_block_editing(&$vars) {
  $block = $vars['block'];

  // Display 'edit block' for custom blocks.
  if ($block->module == 'block') {
    $edit_links[] = l(t('Edit block'), 'admin/build/block/configure/' . $block->module . '/' . $block->delta,
      array(
        'attributes' => array(
          'title' => t('edit the content of this block'),
          'class' => 'block-edit-block',
        ),
        'query' => drupal_get_destination(),
        'html' => TRUE,
      )
    );
  }
  // Display 'configure' for other blocks.
  else {
    $edit_links[] = l(t('Configure'), 'admin/build/block/configure/' . $block->module . '/' . $block->delta,
      array(
        'attributes' => array(
          'title' => t('configure this block'),
          'class' => 'block-edit-config',
        ),
        'query' => drupal_get_destination(),
        'html' => TRUE,
      )
    );
  }

  // Display 'edit view' for Views blocks.
  if ($block->module == 'views' && user_access('administer views')) {
    list($view_name, $view_block) = explode('-block', $block->delta);
    $edit_links[] = l(t('Edit view'), 'admin/build/views/edit/' . $view_name,
      array(
        'attributes' => array(
          'title' => t('edit the view that defines this block'),
          'class' => 'block-edit-view',
        ),
        'query' => drupal_get_destination(),
        'fragment' => 'views-tab-block' . $view_block,
        'html' => TRUE,
      )
    );
  }
  // Display 'edit menu' for Menu blocks.
  elseif (($block->module == 'menu' || ($block->module == 'user' && $block->delta == 1)) && user_access('administer menu')) {
    $menu_name = ($block->module == 'user') ? 'navigation' : $block->delta;
    $edit_links[] = l(t('Edit menu'), 'admin/build/menu-customize/' . $menu_name,
      array(
        'attributes' => array(
          'title' => t('edit the menu that defines this block'),
          'class' => 'block-edit-menu',
        ),
        'query' => drupal_get_destination(),
        'html' => TRUE,
      )
    );
  }
  // Display 'edit menu' for Menu block blocks.
  elseif ($block->module == 'menu_block' && user_access('administer menu')) {
    $menu_name = variable_get('menu_block_' . $block->delta . '_menu_name', 'navigation');
    $edit_links[] = l(t('Edit menu'), 'admin/build/menu-customize/' . $menu_name,
      array(
        'attributes' => array(
          'title' => t('edit the menu that defines this block'),
          'class' => 'block-edit-menu',
        ),
        'query' => drupal_get_destination(),
        'html' => TRUE,
      )
    );
  }
  // Display 'edit tabs' for QuickTabs blocks.
  elseif ($block->module == 'quicktabs') {
    $edit_links[] = l(t('Edit quicktabs'), 'admin/build/' . $block->module . '/' . $block->delta . '/' . 'edit',
      array(
        'attributes' => array(
          'title' => t('Edit the tabs of this block.'),
          'class' => 'block-edit-quicktabs',
        ),
        'html' => TRUE,
      )
    );
  }

  $vars['edit_links_array'] = $edit_links;
  $vars['edit_links'] = '<div class="block-edit"><span class="icon"></span><div>' . implode(' ', $edit_links) . '</div></div>';
}
