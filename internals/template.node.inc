<?php
/**
 * @file
 * Contains functions only needed when rendering nodes.
 * Released under the GNU General Public License.
 */

/**
 * Implementation of preprocess_node().
 */
function _pop_preprocess_node(&$vars) {
  $vars['hook'] = 'node';

  $attr = array();
  $attr['id'] = 'node-'. $vars['node']->nid;

  $node_classes[] = 'node node-'. $vars['node']->type;
  $node_classes[] = 'node-'. $vars['zebra'];
  $node_classes[] = $vars['node']->sticky ? 'sticky' : '';
  $node_classes[] = (isset($vars['node']->teaser)) ? 'teaser' : 'full-node';
  $node_classes[] = (!$vars['node']->status) ? 'node-unpublished' : '';

  // Add print customizations
  if (isset($_GET['print'])) {
    $vars['post_object'] = tao_print_book_children($vars['node']);
  }


  // Date & author
  $author = theme('username', $vars['node']);
  $date = format_date($vars['node']->created, 'medium');
  $date_only = t('On ');
  $date_separator = t(' on ');
  $node_content_type = (theme_get_setting('node_by_content_type') == 1) ? $vars['node']->type : 'default';
  $date_setting = (theme_get_setting('node_show_date_'. $node_content_type) == 1);
  $author_setting = (theme_get_setting('node_show_author_'. $node_content_type) == 1);
  $author_date = ($author_setting) ? t('By ') . $author : '';
  $date_separator = ($author_setting) ? $date_separator : $date_only;
  $author_date .= ($date_setting) ? $date_separator . $date : '';

  $vars['author'] = $author;
  $vars['date'] = $date;
  $vars['author_setting'] = $author_setting;
  $vars['date_setting'] = $date_setting;
  $vars['submitted'] = $author_date;

  // Taxonomy
  $taxonomy_content_type = (theme_get_setting('taxonomy_by_content_type') == 1) ? $vars['node']->type : 'default';
  $taxonomy_display = theme_get_setting('taxonomy_display_'. $taxonomy_content_type);
  $taxonomy_format = theme_get_setting('taxonomy_format_'. $taxonomy_content_type);

  $node_classes[] = 'taxonomy-' . $taxonomy_format;
  $vars['taxonomy_format'] = $taxonomy_format;

  if ((module_exists('taxonomy')) && ($taxonomy_display == 'all' || ($taxonomy_display == 'fullnode' && $vars['page']))) {
    $vocabularies = taxonomy_get_vocabularies($vars['node']->type);
    $output = '';
    $vocab_delimiter = '';
    foreach ($vocabularies as $vocabulary) {
      if (theme_get_setting('taxonomy_vocab_display_'. $taxonomy_content_type .'_'. $vocabulary->vid) == 1) {
        $terms = taxonomy_node_get_terms_by_vocabulary($vars['node'], $vocabulary->vid);
        if ($terms) {
          $links = array();
          foreach ($terms as $term) {
            $links[] = l($term->name, taxonomy_term_path($term), array('attributes' => array('rel' => 'tag', 'title' => strip_tags($term->description))));
          }
          if ($taxonomy_format == 'vocab') {
            $output .=  '<div class="vocab vocab-'. $vocabulary->vid .'"><div class="vocab-name">'. $vocabulary->name .':</div> <div class="vocab-list">';
            $output .= implode(', ', $links);
            $output .= '</div></div>';
          }
          else if ($taxonomy_format == 'list') {
            $output .= theme('item_list', $links);
          }
        }
      }
    }
    if ($output != '') {
      $output = '<div class="taxonomy">'. $output .'</div>';
    }
    $vars['terms'] = $output;
  }
  else {
    $vars['terms'] = '';
  }

  // Node Links
  if (isset($vars['node']->links['node_read_more'])) {
    $node_content_type = (theme_get_setting('readmore_by_content_type') == 1) ? $vars['node']->type : 'default';
    $vars['node']->links['node_read_more'] = array(
      'title' => l(
        t(theme_get_setting('readmore_'. $node_content_type)),
        'node/'. $vars['node']->nid,
        array(
          'attributes' => array('title' => t(theme_get_setting('readmore_title_'. $node_content_type))),
          'query' => NULL, 'fragment' => NULL, 'absolute' => FALSE, 'html' => TRUE
          )
      ),
      'attributes' => array('class' => 'readmore-item'),
      'html' => TRUE,
    );
  }
  if (isset($vars['node']->links['comment_add'])) {
    $node_content_type = (theme_get_setting('comment_by_content_type') == 1) ? $vars['node']->type : 'default';
    if ($vars['teaser']) {
      $vars['node']->links['comment_add'] = array(
        'title' => l(
          t(theme_get_setting('comment_add_'. $node_content_type)),
          "comment/reply/".$vars['node']->nid,
          array(
            'attributes' => array('title' => t(theme_get_setting('comment_add_title_'. $node_content_type))),
            'query' => NULL, 'fragment' => 'comment-form', 'absolute' => FALSE, 'html' => TRUE
            )
        ),
        'attributes' => array('class' => 'comment-add-item'),
        'html' => TRUE,
      );
    }
    else {
      $vars['node']->links['comment_add'] = array(
        'title' => l(
          t(theme_get_setting('comment_node_'. $node_content_type)),
          "comment/reply/".$vars['node']->nid,
          array(
            'attributes' => array('title' => t(theme_get_setting('comment_node_title_'. $node_content_type))),
            'query' => NULL, 'fragment' => 'comment-form', 'absolute' => FALSE, 'html' => TRUE
            )
        ),
        'attributes' => array('class' => 'comment-node-item'),
        'html' => TRUE,
      );
    }
  }
  if (isset($vars['node']->links['comment_new_comments'])) {
    $node_content_type = (theme_get_setting('comment_by_content_type') == 1) ? $vars['node']->type : 'default';
    $vars['node']->links['comment_new_comments'] = array(
      'title' => l(
        format_plural(
          comment_num_new($vars['node']->nid),
          t(theme_get_setting('comment_new_singular_'. $node_content_type)),
          t(theme_get_setting('comment_new_plural_'. $node_content_type))
        ),
        "node/".$vars['node']->nid,
        array(
          'attributes' => array('title' => t(theme_get_setting('comment_new_title_'. $node_content_type))),
          'query' => NULL, 'fragment' => 'new', 'absolute' => FALSE, 'html' => TRUE
        )
      ),
      'attributes' => array('class' => 'comment-new-item'),
      'html' => TRUE,
    );
  }
  if (isset($vars['node']->links['comment_comments'])) {
    $node_content_type = (theme_get_setting('comment_by_content_type') == 1) ? $vars['node']->type : 'default';
    $vars['node']->links['comment_comments'] = array(
      'title' => l(
        format_plural(
          comment_num_all($vars['node']->nid),
          t(theme_get_setting('comment_singular_'. $node_content_type)),
          t(theme_get_setting('comment_plural_'. $node_content_type))
        ),
        "node/".$vars['node']->nid,
        array(
          'attributes' => array('title' => t(theme_get_setting('comment_title_'. $node_content_type))),
          'query' => NULL, 'fragment' => 'comments', 'absolute' => FALSE, 'html' => TRUE
        )
      ),
      'attributes' => array('class' => 'comment-item'),
      'html' => TRUE,
    );
  }
  $vars['links'] = theme('links', $vars['node']->links, array('class' => 'links inline'));


  $node_classes = array_filter($node_classes);
  $attr['class'] = implode(' ', $node_classes);
  $vars['attr'] = $attr;
}
