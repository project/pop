<?php
/**
 * @file
 * Contains functions only needed when rendering comments.
 * Released under the GNU General Public License.
 */

/**
 * Implementation of preprocess_comment_wrapper().
 */
function _pop_preprocess_comment_wrapper(&$vars) {
//  $vars['comment_header']  = theme_get_setting('comment_header');
  $vars['hook'] = 'comment-wrapper';
  $vars['title'] = theme_get_setting('comment_header');

  $vars['attr']['id'] = 'comments';
  $vars['attr']['class'] .= ' clear-block';
}

/**
 * Implementation of preprocess_comment().
 */
function _pop_preprocess_comment(&$vars) {
  global $user;
  static $comment_odd = TRUE;

  $vars['hook'] = 'comment';

  // Build an array of comment classes
  $comment_classes = array();
  $comment_classes[] = 'comment clearfix';
  $comment_classes[] = $comment_odd ? 'comment-odd' : 'comment-even';
  $comment_odd = !$comment_odd;

  $comment_classes[] = ($vars['comment']->new) ? 'comment-new' : '';
  $comment_classes[] = ($vars['comment']->status == COMMENT_NOT_PUBLISHED) ? 'comment-unpublished' : '';
  $comment_classes[] = ($vars['comment']->uid == 0) ? 'comment-anonymous' : '';

  $node = node_load($vars['comment']->nid);
  $comment_classes[] = ($vars['comment']->uid == $node->uid) ? 'comment-author' : '';

  $comment_classes = array_filter($comment_classes);
//  $vars['comment_classes'] = implode(' ', $comment_classes);
  $vars['attr']['class'] .= implode(' ', $comment_classes);

  $vars['comment_author'] = '<div class="comment-name">'.  theme('username', $vars['comment']) .'</div>';
  $vars['comment_date'] = '<div class="comment-date">'.  format_date($vars['comment']->timestamp, 'small') .'</div>';
  $vars['submitted'] = t('By !author on !date', array('!author' => $vars['comment_author'], '!date' => $vars['comment_date']));
}

