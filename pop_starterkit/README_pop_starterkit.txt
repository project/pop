// $Id$

NOTES
======
* In Drupal 6, the theme system caches template files and which theme functions should be called. What that means is if you add a new theme or preprocess function to your template.php file or add a new template (.tpl.php) file to your sub-theme, you will need to rebuild the "theme registry." See http://drupal.org/node/173880#theme-registry

* Drupal 6 also stores a cache of the data in .info files. If you modify any lines in your sub-theme's .info file, you MUST refresh Drupal 6's cache by simply visiting the admin/build/themes page.

Create Your Own Sub-theme
===========================
The base Pop theme from forDrupal is designed to be easily extended by its sub-themes. Avoid modify any of the CSS or PHP files in the pop/ folder; Instead you should create a sub-theme of Pop in a folder outside of the root pop/ folder. The examples below assume Pop and your sub-theme will be installed in sites/all/themes/, but any valid theme directory is acceptable.

- Copy the pop_starterkit folder out of the pop/ folder and rename it to be your new sub-theme. IMPORTANT: Only lowercase letters and underscores should be used for the name of your sub-theme.

  For example, copy the sites/all/themes/fordrupal/pop/pop_starterkit folder and rename it as sites/all/themes/foo.

  Why? Each theme should reside in its own folder. To make it easier to upgrade Pop, sub-themes should reside in a folder separate from their base theme.


- In your new sub-theme folder, rename the pop_starterkit.info.txt file to include the name of your new sub-theme and remove the ".txt" extension. Then edit the .info file by editing the name and description field.

  For example, rename the foo/pop_starterkit.info.txt file to foo/foo.info. Edit the foo.info file and change "name = forDrupal Pop SubTheme Starter Kit" to "name = Foo" and to "description = A Pop sub-theme".

  Why? The .info file describes the basic things about your theme: its name, description, features, template regions, CSS files, and JavaScript files.
  See the Drupal 6 Theme Guide for more info: http://drupal.org/node/171205

  Then, visit your site's admin/build/themes to refresh Drupal 6's cache of .info file data.


- Pop is using Sass (http://sass-lang.com/) and Compass (http://compass-style.org/) to improve the stylesheet. It is optional to use Sass/Compass, you can still directly edit the CSS files. It is recommended to install them on your development server together with Compass stylesheet tool (http://drupal.org/project/compass) during your development and disable them on your production site once the CSS files have been generated.

  By using Sass/Compass, you get to apply commonly used CSS patterns (e.g. reset, CSS3) and battle-tested styles from frameworks like Blueprint and/or 960.gs to your stylesheets instead of your markup. Hence you can add in grid support as needed and still have the flexibility to flex your CSS theming skill.

  Variables defined in Pop are listed in pop/scss/_base.scss.
  All these values can be easily overriden by giving it a new value inside your style.scss.

  For example, edit foo/scss/style.scss and add a line:
    $pop-font-family: Georgia, Times New Roman, serif;  // To change the overall font-family
    $body-background-color: #eee                        // To change the body background color

  Regenerate the CSS, if you using Compass module, it will regenerate when theme registry is cleared.


- By default your new Pop sub-theme is using a fixed-width layout. If you want a liquid layout for your theme and you are using Sass/Compass, just define a new value inside your style.scss. Note that you can only using one type of unit at the same time, i.e. if you using percentage %, all have to be in %; if using em, all must be em;

  For example, edit foo/scss/style.scss and add a line:
    $wrapper_width:         90%;  // container width
    $first_sidebar_width:   20%;
    $second_sidebar_width:  20%;

  Regenerate the CSS.


- If you would like to use CSS framework like Blueprint and/or 960.gs, you can install them to work with Sass/Compass. Blueprint is already included with Compass installation. You can obtain 960.gs plugin from http://github.com/chriseppstein/compass-960-plugin.

  Yes, you can mix Blueprint and 960.gs and other CSS framework, or even write your own.


- If you need template.php, make sure all your hook function name start with the name of your sub-theme.

    For example, add foo/template.php and make sure all the hook implementation start with "foo", e.g. foo_theme(), foo_preprocess_page().

- Log in as an administrator on your Drupal site and go to Administer > Site building > Themes (admin/build/themes) and enable your new sub-theme.


Optional:

- MODIFYING TEMPLATE FILES:
  If you decide you want to modify any of the .tpl.php template files in the pop folder, copy them to your subtheme's folder before making any changes. Then rebuild the theme registry.

    For example, copy fusion_core/page.tpl.php to sunshine/page.tpl.php