<?php
// $Id$

// Include the definition of pop_settings() and _pop_get_default_settings().
include_once './' . drupal_get_path('theme', 'pop') . '/theme-settings.php';


/**
 * Implements THEMEHOOK_settings() function.
 *
 * @param $saved_settings
 *   An array of saved settings for this theme.
 * @return
 *   A form array.
 */
function pop_starterkit_settings($saved_settings) {

  // Get the default values from the .info file.
  $defaults = _pop_get_default_settings('pop_starterkit');

  // Merge the saved variables and their default values.
  $settings = array_merge($defaults, $saved_settings);

  /* Create the form */
  $form = array();
  /* Add your own subtheme setting, e.g.
  $form['pop_starterkit_example'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use this sample setting'),
    '#default_value' => $settings['pop_starterkit_example'],
    '#description'   => t("This option doesn't do anything; it's just an example."),
  );
  // */

  // Add the base theme's settings.
  $form += pop_settings($saved_settings, $defaults);

  // Return the form
  return $form;
}
