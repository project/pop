<?php
/* $Id */

/**
 * @file template.php
 *
 * forDrupal Pop subtheme.
 * Released under the GNU General Public License.
 */

/*
 * Tip: Use devel to clean theme registry during development.
 */

/**
 * Implements hook_theme().
 */
function pop_starterkit_theme(&$existing, $type, $theme, $path) {
  $items = array();
  $items = pop_theme($existing, $type, $theme, $path);

  /* Add your theme hooks here, e.g.
  $items['node'] = array(
    'template' => 'mynode',
  );
  // */

  return $items;
}