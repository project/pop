<?php
/* $Id */

/**
 * @file template.php
 *
 * forDrupal Pop base theme.
 * Released under the GNU General Public License.
 */

/*
 * Tip: Use devel to clean theme registry during development.
 */

/*--- Helper functions ---*/
/**
 * Include templates files in theme, under templates subfolder
 */
function _theme_load_include($type, $theme, $name = NULL) {
  if (empty($name)) {
    $name = $theme;
  }

  $file = './'. _get_pop_theme_path($theme) ."/$name.$type";

  if (is_file($file)) {
    require_once $file;
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Returns the path to the theme $theme.
 *
 * drupal_get_filename() is broken; see #341140. When that is fixed in Drupal 6,
 * replace _get_pop_theme_path($theme) with drupal_get_path('theme', $theme).
 */
function _get_pop_theme_path($theme) {
  if (!$path) {
    $matches = drupal_system_listing($theme .'\.info$', 'themes', 'name', 0);
    if (!empty($matches[$theme]->filename)) {
      $path = dirname($matches[$theme]->filename);
    }
  }
  return $path;
}


/**
 * Implements hook_theme().
 */
function pop_theme(&$existing, $type, $theme, $path) {
  require_once('theme-settings.php');
  _pop_theme($theme);

  $items = array();

  // Only load once for base theme.
  if ($theme == 'pop') {
    // Consolidate a variety of theme functions under a single template type.
    $items['block'] =
    $items['box'] =
    $items['comment'] =
    $items['comment_wrapper'] =
    $items['fieldset'] = array(
      'template' => 'container',
      'path' => _get_pop_theme_path('pop'),
    );
    $items['fieldset']['arguments'] = array('element' => array());

    // Print friendly page headers.
    $items['print_header'] = array(
      'arguments' => array(),
      'template' => 'print-header',
      'path' => _get_pop_theme_path('pop'),
    );

    // Make confirm form nicer by moving the long title
    $items['confirm_form'] = array(
      'arguments' => array('form' => array()),
      'template' => 'confirm-form',
      'path' => _get_pop_theme_path('pop'),
      'preprocess functions' => array(
        'pop_preprocess_form_confirm'
      ),
    );
  }

  return $items;
}


/*--- Preprocesses ---*/
/**
 * Implements preprocess_page().
 */
function pop_preprocess_page(&$vars) {
  _theme_load_include('inc', 'pop', 'internals/template.page');
  _pop_preprocess_page($vars);
}

/**
 * Implements preprocess_maintenance_page().
 */
function pop_preprocess_maintenance_page(&$vars) {
  if (db_is_active()) {
    pop_preprocess_page($vars);
  }
}

/**
 * Implements preprocess_node().
 */
function pop_preprocess_node(&$vars) {
  _theme_load_include('inc', 'pop', 'internals/template.node');
  _pop_preprocess_node($vars, $hook);
}

/**
 * Implements preprocess_block().
 */
function pop_preprocess_block(&$vars) {
  // Hide blocks with no content.
  $vars['hide'] = empty($vars['block']->content);

  $attr = array();
  $attr['id'] = "block-{$vars['block']->module}-{$vars['block']->delta}";
  $attr['class'] = 'block block-'. $vars['block']->module;
  $attr['class'] .= $vars['block']->class;

  $vars['hook'] = 'block';
  $vars['title'] = !empty($vars['block']->subject) ? $vars['block']->subject : '';
  $vars['content'] = $vars['block']->content;

  if (!$block->title) {
    $attr['class'] .= ' block-no-title';
  }

  $vars['attr'] = $attr;
}

/**
 * Implements preprocess_box().
 */
function pop_preprocess_box(&$vars) {
  $attr = array();
  $attr['class'] = "box";
  $vars['attr'] = $attr;
  $vars['hook'] = 'box';
}

/**
 * Implements preprocess_comment_wrapper().
 */
function pop_preprocess_comment_wrapper(&$vars) {
  _theme_load_include('inc', 'pop', 'internals/template.comment');
  _pop_preprocess_comment_wrapper($vars);
}

/**
 * Implements preprocess_comment().
 */
function pop_preprocess_comment(&$vars) {
  _theme_load_include('inc', 'pop', 'internals/template.comment');
  _pop_preprocess_comment($vars);
}


/**
 * Implements preprocess_search_theme_form().
 */
function pop_preprocess_search_theme_form(&$vars) {
  global $user;
  if (theme_get_setting('search_box_label')) {
    // Modify label of the search form
    $vars['form']['search_theme_form']['#title'] = t(theme_get_setting('search_box_label_text'));
  }
  else {
    // remove the label
    unset($vars['form']['search_theme_form']['#title']);
  }

  if (theme_get_setting('search_box_text')) {
    $search_box_text = theme_get_setting('search_box_text') == "<none>" ? '' : t(theme_get_setting('search_box_text'));
    // Set a value for the search box
    $vars['form']['search_theme_form']['#value'] = $search_box_text;

    // Add a clear button
    $vars['form']['search_theme_form']['#field_suffix'] = '<span class="text-clear"></span>';

    // Add a custom js code to the search box
    if ($search_box_text !== '') {
      // TODO: use CSS placehold & placeheld.js
      drupal_add_js(array('pop' => array('search_box_text' => $search_box_text)), 'setting');
      $vars['form']['search_theme_form']['#attributes']['class'] = 'search-blur';
    }
    $vars['form']['search_theme_form']['#attributes']['title'] = t(theme_get_setting('search_box_tooltip_text'));
  }

  if (theme_get_setting('search_box_button')) {
    // Change the text on the submit button
    $vars['form']['submit']['#value'] = t(theme_get_setting('search_box_button_text'));

    if (theme_get_setting('search_box_button_image')) {
      global $theme_key;

      $vars['form']['submit']['#type'] = 'image_button';
      $vars['form']['submit']['#src'] = drupal_get_path('theme', $theme_key) . '/images/search.png';
    }
  }
  else {
    unset($vars['form']['submit']);
  }

  // Rebuild the rendered version (search form only, rest remains unchanged)
  unset($vars['form']['search_theme_form']['#printed']);
  $vars['search']['search_theme_form'] = drupal_render($vars['form']['search_theme_form']);

  // Rebuild the rendered version (submit button, rest remains unchanged)
  unset($vars['form']['submit']['#printed']);
  $vars['search']['submit'] = drupal_render($vars['form']['submit']);

  // Collect all form elements to make it easier to print the whole form.
  $vars['search_form'] = implode($vars['search']);
}

/**
 * Implements preprocess_fieldset().
 */
function pop_preprocess_fieldset(&$vars) {
  $element = $vars['element'];

  $attr = isset($element['#attributes']) ? $element['#attributes'] : array();
  $attr['class'] = empty($attr['class']) ? 'fieldset' : $attr['class'] .' fieldset';
  $attr['class'] .= empty($element['#collapsible']) ? '' :  ' collapsible';
  $attr['class'] .= empty($element['#collapsible']) && empty($element['#collapsed']) ? '' : ' collapsed';
  $vars['attr'] = $attr;

  $description = empty($element['#description']) ? '' : '<div class="description">'. $element['#description'] .'</div>';
  $children = empty($element['#children']) ? '' : $element['#children'];
  $value = empty($element['#value']) ? '' : $element['#value'];
  $vars['content'] = $description . $children . $value;
  $vars['title'] = empty($element['#title']) ? '' : $element['#title'] ;

  if (!empty($element['#collapsible'])) {
    $vars['title'] = l($vars['title'], $_GET['q'], array('fragment' => 'fieldset'));
  }

  $vars['hook'] = 'fieldset';
}

/**
 * Preprocessor for theme_print_header().
 */
function pop_preprocess_print_header(&$vars) {
  $vars = array(
    'base_path' => base_path(),
    'theme_path' => base_path() .'/'. path_to_theme(),
    'site_name' => variable_get('site_name', 'Drupal'),
  );
}

/**
 * Preprocessor for theme('confirm_form').
 */
function pop_preprocess_form_confirm(&$vars) {
  // Remove the title from the page title
  $title = filter_xss_admin(drupal_get_title());
  $text = empty($vars['form']['description']['#value']) ?
    '<strong>'. $title .'</strong>' :
    '<strong>'. $title .'</strong><p>'. $vars['form']['description']['#value'] .'</p>';
  $vars['form']['description'] = array(
    '#type' => 'item',
    '#value' => $text,
  );
  drupal_set_title(t('Confirmation'));
}

/*--- Functions override ---*/
/**
 * Override blocks first and last classes
 */
function pop_blocks($region) {
  $output = '';

  if ($list = block_list($region)) {
    $counter = 1;
    foreach ($list as $key => $block) {
      $block->class = '';
      $block->class .= ( $counter == 1 ? ' block-first' : '' );
      $block->class .= ( $counter == count($list) ? ' block-last' : '' );
      $output .= theme('block', $block);
      $counter++;
    }
  }

  // Add any content assigned to this region through drupal_set_content() calls.
  $output .= drupal_get_content($region);

  return $output;
}

/**
 * Override username theming to display/hide 'not verified' text
 */
function pop_username($object) {
  if ($object->uid && $object->name) {
    // Shorten the name when it is too long or it will break many tables.
    if (drupal_strlen($object->name) > 20) {
      $name = drupal_substr($object->name, 0, 15) .'&hellip;';
    }
    else {
      $name = $object->name;
    }
    if (user_access('access user profiles')) {
      $output = l($name, 'user/'. $object->uid, array('attributes' => array('title' => t('View user profile.'))));
    }
    else {
      $output = check_plain($name);
    }
  }
  else if ($object->name) {
    // Sometimes modules display content composed by people who are
    // not registered members of the site (e.g. mailing list or news
    // aggregator modules). This clause enables modules to display
    // the true author of the content.
    if (!empty($object->homepage)) {
      $output = l($object->name, $object->homepage, array('attributes' => array('rel' => 'nofollow')));
    }
    else {
      $output = check_plain($object->name);
    }
    // Display or hide 'not verified' text
    if (theme_get_setting('comment_user_notverified') == 1) {
      $output .= ' ('. t('not verified') .')';
    }
  }
  else {
    $output = variable_get('anonymous', t('Anonymous'));
  }
  return '<span class="username">'. $output .'</span>';
}

/**
 * Override of theme_menu_local_tasks().
 * Add argument to allow primary/secondary local tasks to be printed
 * separately. Use theme_links() markup to consolidate.
 */
function pop_menu_local_tasks($type = '') {
  if ($primary = menu_primary_local_tasks()) {
    $primary = '<ul class="links primary-tabs">'. $primary .'</ul>';
  }
  if ($secondary = menu_secondary_local_tasks()) {
    $secondary = '<ul class="links secondary-tabs">'. $secondary .'</ul>';
  }
  switch ($type) {
    case 'primary':
      return $primary;
    case 'secondary':
      return $secondary;
    default:
      return $primary . $secondary;
  }
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function pop_breadcrumb($breadcrumb) {
  // Determine if we are to display the breadcrumb
  $show_breadcrumb = theme_get_setting('breadcrumb');
  if ($show_breadcrumb == 'yes' || $show_breadcrumb == 'admin' && arg(0) == 'admin') {

    // Optionally get rid of the homepage link
    $show_breadcrumb_home = theme_get_setting('breadcrumb_home');
    if (!$show_breadcrumb_home) {
      array_shift($breadcrumb);
    }

    // Return the breadcrumb with separators
    if (!empty($breadcrumb)) {
      $breadcrumb_separator = theme_get_setting('breadcrumb_separator');
      $trailing_separator = (theme_get_setting('breadcrumb_trailing') || theme_get_setting('breadcrumb_title')) ? $breadcrumb_separator : '';
      return '<div class="breadcrumb">' . implode($breadcrumb_separator, $breadcrumb) . $trailing_separator . '</div>';
    }
  }
  // Otherwise, return an empty string
  return '';
}

/**
 * theme_node_preview override
 *
 * @param $node
 *   The node object which is being previewed.
 */
function pop_node_preview($node) {
  $output = '<div class="preview">';

  $preview_trimmed_version = FALSE;
  // Do we need to preview trimmed version of post as well as full version?
  if (isset($node->teaser) && isset($node->body)) {
    $teaser = trim($node->teaser);
    $body = trim(str_replace('<!--break-->', '', $node->body));

    // Preview trimmed version if teaser and body will appear different;
    // also (edge case) if both teaser and body have been specified by the user
    // and are actually the same.
    if ($teaser != $body || ($body && strpos($node->body, '<!--break-->') === 0)) {
      $preview_trimmed_version = TRUE;
    }
  }

  if ($preview_trimmed_version) {
    drupal_set_message(t('The trimmed version of your post shows what your post looks like when promoted to the main page or when exported for syndication.<span class="no-js"> You can insert the delimiter "&lt;!--break--&gt;" (without the quotes) to fine-tune where your post gets split.</span>'));
    $output .= '<h3 class="preview-title">'. t('Preview trimmed version') .'</h3>';
    $output .= node_view(drupal_clone($node), 1, FALSE, 0);
    $output .= '<h3 class="preview-title">'. t('Preview full version') .'</h3>';
    $output .= node_view($node, 0, FALSE, 0);
  }
  else {
    $output .= node_view($node, 0, FALSE, 0);
  }
  $output .= '</div>';

  return $output;
}

