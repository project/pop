<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <?php print $head ?>
  <title><?php print $head_title ?></title>
  <?php print $styles ?>
  <?php print $scripts ?>
</head>
<body <?php print drupal_attributes($attr) ?>>
<div id="wrapper">
  <div id="header" class="clearfix">
    <?php print $header ?>

    <div class="breadcrumb clearfix"><?php print $breadcrumb ?></div>
    <?php if ($user_links): ?>
      <div class="user-links clearfix">
        <?php print theme('links', $user_links) ?>
      </div>
    <?php endif; ?>
    <?php if ($secondary_links): ?>
      <div class="secondary-links">
        <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
      </div>
    <?php endif; ?>

    <?php if ($logo): ?>
      <div class="logo">
        <a href="<?php print check_url($front_page) ?>" title="<?php print check_plain($site_name) ?>" rel="home">
          <img src="<?php print check_url($logo) ?>" alt="<?php print check_plain($site_name) ?>" id="logo" />
        </a>
      </div>
    <?php endif; ?>
    <?php if ($site_name || $site_slogan): ?>
      <div class="site-name-slogan">
        <?php if ($site_name): ?>
          <h1><a href="<?php print check_url($front_page) ?>" title="<?php print check_plain($site_name) ?>" rel="home">
            <?php print check_plain($site_name) ?>
          </a></h1>
        <?php endif; ?>
        <?php if ($site_slogan): ?>
          <div class="slogan"><?php print $site_slogan ?></div>
        <?php endif; ?>
      </div>
    <?php endif; ?>
  </div>

  <div id="page-title" class="clearfix">
    <?php if ($title): ?>
      <h2 class='page-title'>
        <?php print $title ?>
      </h2>
    <?php endif; ?>
    <?php if ($primary_links_tree): ?>
      <div class="primary-links">
        <?php print $primary_links_tree ?>
      </div>
    <?php endif; ?>
    <?php if ($search_box): ?>
      <div class="search-box">
        <?php print $search_box; ?>
      </div><!-- /search-box -->
    <?php endif; ?>
  </div>

  <div id="page" class="clearfix">
    <div id="content">
      <div id="content-header">
        <?php if ($tabs): ?>
          <div class="primary-tabs clearfix"><?php print $tabs ?></div>
        <?php endif; ?>
        <?php if ($tabs2): ?>
          <div class="secondary-tabs clearfix"><?php print $tabs2 ?></div>
        <?php endif; ?>
        <?php if ($help && $help_toggle) print $help_toggle ?>
        <?php if ($help) print $help ?>
        <?php if ($show_messages && $messages && $messages_toggle) print $messages_toggle ?>
        <?php if ($show_messages && $messages): ?>
          <?php print $messages; ?>
        <?php endif; ?>
      </div>

      <?php if (!empty($content)): ?>
        <div class="content-wrapper clearfix"><?php print $content ?></div>
      <?php endif; ?>
      <?php print $content_region ?>
    </div>

    <?php if ($sidebar_first): ?>
      <div id="first-sidebar">
        <?php print $sidebar_first ?>
      </div>
    <?php endif; ?>

    <?php if ($sidebar_second): ?>
      <div id="second-sidebar">
        <?php print $sidebar_second ?>
      </div>
    <?php endif; ?>
  </div>

  <div id="footer" class="clearfix">
    <?php if ($feed_icons): ?>
      <div class="feed-icons clearfix">
        <label><?php print t('Feeds') ?></label>
        <?php print $feed_icons ?>
      </div>
    <?php endif; ?>
    <?php if ($footer_message): ?>
      <div class="footer-message"><?php print $footer_message ?></div>
      <div class="footer"><?php print $footer ?></div>
    <?php endif; ?>
  </div>
</div>

<?php print $closure ?>
</body>
</html>
