/* $Id */

/**
 * @file
 * forDrupal theme settings javascript.
 * Copyright 2010 forDrupal. All rights reserved.
 */

$(function() {
  // Hide the breadcrumb details, if no breadcrumb.
  $('#edit-breadcrumb').change(
    function() {
      var breadcrumb_div = $('.div-breadcrumb-collapse');
      if ($('#edit-breadcrumb').val() == 'no') {
        breadcrumb_div.each(function() {
          $(this).parents('li:first').slideUp('fast');
        });
      }
      else {
        breadcrumb_div.each(function() {
          if ($(this).parents('li:first').css('display') == 'none')
            $(this).parents('li:first').slideDown('fast');
        });
      }
    }
  );
  if ($('#edit-breadcrumb').val() == 'no') {
    $('.div-breadcrumb-collapse').css('display', 'none');
  }
  $('#edit-breadcrumb-title').change(
    function() {
      var checkbox = $('#edit-breadcrumb-trailing');
      if ($('#edit-breadcrumb-title').attr('checked')) {
        checkbox.attr('disabled', 'disabled');
      }
      else {
        checkbox.removeAttr('disabled');
      }
    }
  );
  $('#edit-breadcrumb-title').change();

  // Hide the per content type details
  perContentType('node');
  perContentType('taxonomy');
  perContentType('readmore');
  perContentType('comment');
});

var perContentType = function(item) {
  var custom_checkbox = $('#edit-' + item + '-by-content-type');
  var div = $('.div-' + item + '-collapse');
  var default_fieldset = $('#pop-' + item + '-default legend a');

  custom_checkbox.change(
    function() {
      if (custom_checkbox.attr('checked')) {
        default_fieldset.trigger('click');
        div.slideDown('fast');
      }
      else if (div.css('display') != 'none') {
        default_fieldset.trigger('click');
        div.slideUp('fast');
      }
    }
  );
  if (!custom_checkbox.attr('checked')) {
    div.css('display', 'none');
  }
}