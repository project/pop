/* $Id */

/**
 * @file
 * forDrupal theme javascript.
 * Copyright 2010 forDrupal. All rights reserved.
 */

/*--- Fieldset Collapse ---*/
Drupal.behaviors.popFieldset = function (context) {
  $('.fieldset:not(.pop-processed)').each(function() {
    var fieldset = $(this);

    fieldset.addClass('pop-processed');
    if (fieldset.is('.collapsible')) {
      // Expand if there are errors inside
      if ($('input.error, textarea.error, select.error', fieldset).size() > 0) {
        fieldset.removeClass('collapsed');
      }

      // collapse fieldset
      fieldset.children('.fieldset-title').click(function() {
        var fieldsetTitle = $(this);
        if (fieldsetTitle.parent().is('.collapsed')) {
          fieldsetTitle.siblings('.fieldset-content').show();
          fieldsetTitle.parent().removeClass('collapsed');
        }
        else {
          fieldsetTitle.siblings('.fieldset-content').hide();
          fieldsetTitle.parent().addClass('collapsed');
        }
        return false;
      });
    }
  });
}


/*--- Superfish Menu ---*/
Drupal.behaviors.popMenu = function(context) {
  $('ul.sf-menu').superfish({
    dropShadows:  false,
    delay:        400,
    animation:    {opacity:'show',height:'show',width:'show'},
    speed:        'fast',
    onShow:        function(){ $(this).siblings('a.sf-with-ul').addClass('aHover')},
    onHide:        function(){ $(this).siblings('a.sf-with-ul').removeClass('aHover')}
  });
};


/*--- Search Box Inline Text ---*/
Drupal.behaviors.popSearchBoxText = function(context) {
  if (Drupal.settings.pop && Drupal.settings.pop.search_box_text) {
    var searchbox = $('#search-theme-form input.form-text');
    searchbox.focus(function () {
      if (this.value == Drupal.settings.pop.search_box_text) {
        this.value = '';
        searchbox.removeClass('search-blur');
      }
    })
    .blur(function () {
      if (this.value == '') {
        this.value = Drupal.settings.pop.search_box_text;
        searchbox.addClass('search-blur');
      }
    });
  }
};


/*--- Pre/Code Block Smart Width ---*/
Drupal.behaviors.popCodeWidth = function(context) {
  var maxWidth = $('#wrapper').width();

  $('pre', context).each(function() {
    var contentWidth = $(this).attr('scrollWidth');
    var containerWidth = $(this).outerWidth();

    if (contentWidth > containerWidth) {
      $(this).hover(
        function() {
      		$(this).animate({ width: maxWidth }, 200, function() {
        		if (contentWidth > maxWidth) {
        		  $(this).css({ 'overflow-x': 'scroll' });
        		}
      		});
      	},
      	function() {
      		$(this).css({ 'overflow-x': 'hidden' }).animate({ width: containerWidth }, 200);
    	  }
  	  );
    }
  });
}


/*--- Help Toggle ---*/
Drupal.behaviors.popToggle = function(context) {
  Drupal.popToggle.attach(context);
}

Drupal.popToggle = {
  'targets': [],

  'attach': function(context) {
    $('.toggle:not(.pop-processed)', context).each(function() {
      var toggle = $(this);
      var target = $('.' + toggle.attr("hash").substring(1)); // look for target class
      Drupal.popToggle.targets.push(target);

      target.addClass('pop-toggle-target');

      toggle.addClass('pop-processed')
        .click(function() {
          //Drupal.popToggle.hideTargets(target);
          if (target.hasClass('toggle-open')) {
            target.removeClass('toggle-open')
              .slideUp('fast')
              .addClass('toggle-hidden');
          }
          else {
            target.slideDown('fast')
              .removeClass('toggle-hidden')
              .addClass('toggle-open');
          }

          return false;
        });
    });
  },

  'hideTargets': function(except) {
    for (var i in this.targets) {
      if (this.targets[i] != except) {
        this.targets[i].hide();
      }
    }
  }
};