<?php
/* $Id */

/**
 * @file container.tpl.php
 *
 * Consolidated template for container object including block, box, comment, fieldset.
 *
 * Released under the GNU General Public License.
 */
?>

<?php if (empty($hide)): ?>

<div <?php if (!empty($attr)) print drupal_attributes($attr) ?>>
  <?php if (!empty($title)): ?>
    <h2 class='<?php print $hook ?>-title'>
      <?php if (!empty($new)): ?><a id='new' class='new'><?php print('New') ?></a><?php endif; ?>
      <?php print $title ?>
    </h2>
  <?php endif; ?>

  <?php if (!empty($submitted)): ?>
    <div class="<?php print $hook ?>-submitted clearfix"><?php print $submitted ?></div>
  <?php endif; ?>

  <?php if (!empty($content)): ?>
    <div class="<?php print $hook ?>-content clearfix">
      <?php print $content ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($links)): ?>
    <div class="<?php print $hook ?>-links clearfix"><?php print $links ?></div>
  <?php endif; ?>

  <?php if (!empty($edit_links)): ?>
    <?php print $edit_links; ?>
  <?php endif; ?>
</div>

<?php endif; ?>